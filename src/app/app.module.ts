import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { MatButtonModule } from '@angular/material';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { NavbarModule } from './components/navbar/navbar.module';
import { HomeModule } from './components/home/home.module';
import { EventsModule } from './components/events/events.module';
import { EventModule } from './components/event/event.module';
import { LoginModule } from './components/login/login.module';
import { SignupModule } from './components/signup/signup.module';
import { ProfileModule } from './components/profile/profile.module';
import { UpdateProfileModule } from './components/update-profile/update-profile.module';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    MatButtonModule,
    NavbarModule,
    HomeModule,
    EventsModule,
    EventModule,
    LoginModule,
    SignupModule,
    ProfileModule,
    UpdateProfileModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
