import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../../models/user';
import { environment } from '../../../environments/environment';

const API = environment.protocol + '://' + environment.domain + ':' + environment.port + '/api';

@Injectable()
export class AuthService {

  constructor( private http: HttpClient ) { }

  public authenticate(email: String, password: String) {
    return this.http.post(`${API}/login`, {"email": email, "password": password});
  }

}
