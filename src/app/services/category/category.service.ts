import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Category } from '../../models/category';

const API = 'http://localhost:8000/api';

@Injectable()
export class CategoryService {

  constructor(
    private http: HttpClient
  ) { }

  public createCategory(category: Category) {
    return this.http.post<Category>(`${API}/categories`, category);
  }

  public getCategory(id: number) {
    return this.http.get<Category>(`${API}/categories/${id}`);
  }

  public getCategories() {
    return this.http.get<Array<Category>>(`${API}/categories`);
  }
}
