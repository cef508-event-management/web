import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subscriber } from '../../models/subscriber';

const API = 'http://localhost:8000/api';

@Injectable()
export class SubscriberService {

  constructor(
    private http: HttpClient
  ) { }

  public createSubscriber(subscriber: Subscriber) {
    return this.http.post<Subscriber>(`${API}/subscribers`, subscriber);
  }

  public updateSubscriber(subscriber: Subscriber) {
    return this.http.put<Subscriber>(`${API}/subscribers/${subscriber.id}`, subscriber);
  }

  public getSubscriber(id: number) {
    return this.http.get<Subscriber>(`${API}/subscribers/${id}`);
  }

  public getSubscribers() {
    return this.http.get<Array<Subscriber>>(`${API}/subscribers`);
  }
}
