import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Media } from '../../models/media';

const API = 'http://localhost:8000/api';

@Injectable()
export class MediaService {

  constructor(
    private http: HttpClient
  ) { }

  public createMedia(media: Media) {
    return this.http.post<Media>(`${API}/media`, media);
  }

  public getMedium(id: number) {
    return this.http.get<Media>(`${API}/media/${id}`);
  }

  public getMedia() {
    return this.http.get<Array<Media>>(`${API}/media`);
  }
}
