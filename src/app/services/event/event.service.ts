import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Event } from '../../models/event';
import { environment } from '../../../environments/environment';

const API = environment.protocol + '://' + environment.domain + ':' + environment.port + '/api';

@Injectable()
export class EventService {

  constructor(
    private http: HttpClient
  ) { }

  public createEvent(event: Event) {
    return this.http.post<Event>(`${API}/events`, event);
  }

  public getEvent(id: number) {
    return this.http.get<Event>(`${API}/events/${id}`);
  }

  public getEvents() {
    return this.http.get<Array<Event>>(`${API}/events`);
  }

  public getUserEvents() {
    let token = localStorage.getItem("token");
    return this.http.get<Array<Event>>(`${API}/user-events?api_token=${token}`);
  }

  public getUserEvent(id: number) {
    return this.http.get<Event>(`${API}/user-events/${id}`);
  }
}
