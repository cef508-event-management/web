import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Creator } from '../../models/creator';
import { environment } from '../../../environments/environment';

const API = environment.protocol + '://' + environment.domain + ':' + environment.port + '/api';

@Injectable()
export class CreatorService {

  constructor(
    private http: HttpClient
  ) { }

  public createCreator(creator: Creator) {
    return this.http.post<Creator>(`${API}/creators`, creator);
  }

  public updateCreator(creator: Creator) {
    return this.http.put<Creator>(`${API}/creators/${creator.id}`, creator);
  }

  public getCreator(id: number) {
    return this.http.get<Creator>(`${API}/creators/${id}`);
  }

  public getCreators() {
    return this.http.get<Array<Creator>>(`${API}/creators`);
  }
}
