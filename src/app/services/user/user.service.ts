import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../../models/user';
import { environment } from '../../../environments/environment';

const API = environment.protocol + '://' + environment.domain + ':' + environment.port + '/api';

@Injectable()
export class UserService {

  constructor(
    private http: HttpClient
  ) { }

  public createUser(user: User) {
    return this.http.post<User>(`${API}/users`, user);
  }

  public updateUser(user: User) {
    return this.http.put<User>(`${API}/users/${user.id}`, user);
  }

  public getUser(id: number) {
    return this.http.get<User>(`${API}/users/${id}`);
  }

  public getUsers() {
    return this.http.get<Array<User>>(`${API}/users`);
  }
}
