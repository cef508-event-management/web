import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Ticket } from '../../models/ticket';

const API = 'http://localhost:8000/api';

@Injectable()
export class TicketService {

  constructor(
    private http: HttpClient
  ) { }

  public createTicket(ticket: Ticket) {
    const token = localStorage.getItem('token');

    return this.http.post<any>(`${API}/tickets?api_token=${token}`, ticket);
  }

  public getTicket(id: number) {
    return this.http.get<Ticket>(`${API}/tickets/${id}`);
  }

  public getTickets() {
    return this.http.get<Array<Ticket>>(`${API}/tickets`);
  }

  public getUserTicket(eventId: number) {
    const token = localStorage.getItem('token');

    return this.http.get<Ticket>(`${API}/user-ticket/${eventId}?api_token=${token}`);
  }

  public removeTicket(ticketId: number) {
    return this.http.delete(`${API}/tickets/${ticketId}`);
  }
}
