import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EventService } from '../../services/event/event.service';
import { CreatorService } from '../../services/creator/creator.service';
import { CategoryService } from '../../services/category/category.service';
import { UserService } from '../../services/user/user.service';
import { TicketService } from '../../services/ticket/ticket.service';
import { Event } from '../../models/event';
import { Ticket } from '../../models/ticket';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.css']
})
export class EventComponent implements OnInit {
  event = {} as Event;
  ticket = {} as Ticket;
  category: String = '';
  author: String = '';

  constructor(
    private route: ActivatedRoute,
    private eventService: EventService,
    private creatorService: CreatorService,
    private categoryService: CategoryService,
    private userService: UserService,
    private ticketService: TicketService,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    this.route.params.subscribe((params) => {
      const id = params['id'];

      this.eventService.getEvent(id).subscribe((event) => {
        this.event = { ...event };

        this.creatorService.getCreator(this.event.creator_id).subscribe((creator) => {
          const userId = creator.user_id;

          this.userService.getUser(userId).subscribe((user) => {
            this.author = user.name;
          });
        });

        this.categoryService.getCategory(this.event.category_id).subscribe((category) => {
          this.category = category.name;
        });

        this.ticketService.getUserTicket(this.event.id).subscribe((ticket) => {
          this.ticket = ticket;
          console.log(this.ticket);
        });
      });
    });
  }

  subscribe() {
    const ticket = { } as Ticket;
    ticket.event_id = this.event.id;

    this.ticketService.createTicket(ticket).subscribe((result) => {
      this.ticket = { ...result.ticket };
      console.log(this.ticket);
      this.snackBar.open('You have successfully booked the event', null, {
        duration: 5000,
      });
    }, (err) => {
      this.snackBar.open('Unable to book event', 'CLOSE');
      console.log(err);
    });
  }

  unsubscribe() {
    this.ticketService.removeTicket(this.ticket.id).subscribe((result) => {
      this.snackBar.open('Action completed', null, {
        duration: 5000,
      });
    }, (err) => {
      this.snackBar.open('Unable to complete action', 'CLOSE');
      console.log(err);
    });
  }
}
