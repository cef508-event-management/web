import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import {
    MatGridListModule,
    MatCardModule,
    MatButtonModule,
    MatIconModule,
    MatDividerModule,
    MatTabsModule,
    MatSnackBarModule
} from '@angular/material';

import { EventComponent } from './event.component';
import { EventService } from '../../services/event/event.service';
import { CreatorService } from '../../services/creator/creator.service';
import { CategoryService } from '../../services/category/category.service';
import { UserService } from '../../services/user/user.service';
import { TicketService } from '../../services/ticket/ticket.service';

@NgModule({
    declarations: [ EventComponent ],
    imports: [
        BrowserModule,
        RouterModule,
        MatGridListModule,
        MatCardModule,
        MatButtonModule,
        MatIconModule,
        MatDividerModule,
        MatTabsModule,
        MatSnackBarModule
    ],
    providers: [
        EventService,
        CreatorService,
        CategoryService,
        UserService,
        TicketService
    ],
    exports: [ EventComponent ]
})
export class EventModule {}
