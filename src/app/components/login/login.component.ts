import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';

import { AuthService } from '../../services/auth/auth.service';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public email: String;
  public password: String;
  public isAuthenticated: Boolean = true;
  public sentRequest: Boolean = false;
  public error: String = "";
  public matcher = new MyErrorStateMatcher();

  private emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);

  private passwordFormControl = new FormControl('', [
    Validators.required
  ])

  constructor(
    private router: Router,
    private authService: AuthService
  ) { }

  ngOnInit() {
    console.log("Initializing login component");
  }

  public authenticate(){
    if(this.email && this.password && this.validateEmail(this.email as string) ) {
      this.sentRequest = true;
      this.authService.authenticate(this.email, this.password)
      .subscribe((response) => {
        this.successfullyAuthUser(response);
      }, error => {
        this.isAuthenticated = false;
        this.error = error.error.message;
        console.log(error);
      });
    }
  }

  private validateEmail(email: string) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  private successfullyAuthUser( data ) {
    this.isAuthenticated = false;
    localStorage.removeItem('token');
    localStorage.removeItem('user');
    localStorage.setItem('token', data.token);
    localStorage.setItem('user', JSON.stringify(data.data));
    this.router.navigate(['']);
  }

}
