import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
    MatCardModule,
    MatInputModule,
    MatChipsModule,
    MatListModule,
    MatButtonModule
} from '@angular/material';

import { LoginComponent } from './login.component';
import {AuthService} from '../../services/auth/auth.service'

@NgModule({
    declarations: [ LoginComponent ],
    imports: [
        RouterModule,
        MatCardModule,
        MatInputModule,
        MatChipsModule,
        MatListModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        MatButtonModule
    ],
    providers: [
        AuthService
    ],
    exports: [ LoginComponent ]
})
export class LoginModule {}