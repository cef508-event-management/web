import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import {
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatButtonModule,
    MatDividerModule
} from '@angular/material';

import { HomeComponent } from './home.component';

@NgModule({
    declarations: [ HomeComponent ],
    imports: [
        RouterModule,
        MatCardModule,
        MatFormFieldModule,
        MatInputModule,
        MatButtonModule,
        MatIconModule,
        MatDividerModule
    ],
    exports: [ HomeComponent ]
})
export class HomeModule {}
