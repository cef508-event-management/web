import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
    MatCardModule,
    MatInputModule,
    MatButtonModule,
    MatSelectModule,
    MatStepperModule,
    MatSnackBarModule
} from '@angular/material';

import { UpdateProfileComponent } from './update-profile.component';
import { UserService } from '../../services/user/user.service';
import { CreatorService } from '../../services/creator/creator.service';
import { SubscriberService } from '../../services/subscriber/subscriber.service';

@NgModule({
    declarations: [ UpdateProfileComponent ],
    imports: [
      MatCardModule,
      MatInputModule,
      MatButtonModule,
      MatSelectModule,
      CommonModule,
      RouterModule,
      FormsModule,
      ReactiveFormsModule,
      MatStepperModule,
      MatSnackBarModule
    ],
    providers: [
        CreatorService,
        SubscriberService,
        UserService
    ],
    exports: [ UpdateProfileComponent ]
})
export class UpdateProfileModule {}
