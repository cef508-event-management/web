import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { FormBuilder, FormGroup } from '@angular/forms'
import { Router } from '@angular/router';

import { User } from '../../models/user';
import { Creator } from '../../models/creator';
import { Subscriber } from '../../models/subscriber';
import { UserService } from '../../services/user/user.service';
import { CreatorService } from '../../services/creator/creator.service';
import { SubscriberService } from '../../services/subscriber/subscriber.service';

@Component({
  selector: 'app-update-profile',
  templateUrl: './update-profile.component.html',
  styleUrls: ['./update-profile.component.css']
})
export class UpdateProfileComponent implements OnInit {

  public user = {} as User;
  public role = {};
  public hasRole = false;
  public profileFormGroup: FormGroup;
  public passwordFormGroup: FormGroup;

  constructor(
    private userService: UserService,
    private creatorService: CreatorService,
    private subscriberService: SubscriberService,
    private _formBuilder: FormBuilder,
    public snackBar: MatSnackBar,
    public router: Router
  ) {
    console.log("Initializing update user profile")
  }

  ngOnInit() {
    this.profileFormGroup = this._formBuilder.group({});
    this.passwordFormGroup = this._formBuilder.group({});
    this.user = JSON.parse(localStorage.getItem("user"));
    if(this.user.is_creator) {
      this.hasRole = true;
      this.setCreator();
    } else if (this.user['subscriber']) {
      this.hasRole = true;
      this.setSubscriber();
    }
  }

  private setCreator() {
    this.creatorService.getCreator(this.user.id).subscribe(
      (data) => {
        this.role = data;
      }, (error) => {
        console.log(error);
      }
    );
  }

  private setSubscriber() {
    this.subscriberService.getSubscriber(1).subscribe(
      (data) => {
        this.role = data;
      }, (error) => {
        console.log(error);
      }
    );
  }

  public updateProfile() {
    if (this.user.name && this.user.email) {
      if (this.hasRole) {
        if (this.user.is_creator) {
          this.updateCreator();
        } else {
          this.updateSubscriber();
        }
      }
      this.userService.updateUser(this.user).subscribe(
        (data) => {
          this.displayResponse("Successfuly updated your profile.")
          this.logout();
        }, (error) => {
          console.log(error);
        }
      );
    }
  }

  private updateCreator() {
    this.creatorService.updateCreator(this.role as Creator).subscribe(
      (data) => { }, (error) => {
        console.log(error)
      }
    );
  }

  private updateSubscriber() {
    this.subscriberService.updateSubscriber(this.role as Subscriber).subscribe(
      (data) => { }, (error) => {
        console.log(error)
      }
    );
  }

  public updatePassword() {
    console.log("Update password")
  }

  logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('user');
    this.router.navigate(["/login"]);
  }

  public displayResponse(message: string) {
    this.snackBar.open(message, "CLOSE", {
      duration: 5000,
    });
  }

}
