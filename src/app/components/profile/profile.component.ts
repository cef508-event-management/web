import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { MatPaginator, MatTableDataSource } from '@angular/material';

import { User } from '../../models/user';
import { Event } from '../../models/event';
import { EventService } from '../../services/event/event.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  public user = {} as User;
  public displayedColumns: string[] = ['title', 'date', 'location'];
  public dataSource = new MatTableDataSource<Event>();
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private router: Router, private eventService: EventService) {
    console.log("Initializing user profile")
  }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.user = JSON.parse(localStorage.getItem('user'));
    this.setEvents();
  }

  private setEvents() {
    this.eventService.getUserEvents().subscribe(
      (data) => {
        this.dataSource.data = data;
      }, (error) => {
        console.log(error);
      }
    );
  }

  public redirect(url: String) {
    this.router.navigate(["/events", url])
  }

}
