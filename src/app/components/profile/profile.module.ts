import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
    MatCardModule,
    MatInputModule,
    MatChipsModule,
    MatListModule,
    MatButtonModule,
    MatIconModule,
    MatTableModule,
    MatPaginatorModule
} from '@angular/material';

import { ProfileComponent } from './profile.component';
import { EventService } from '../../services/event/event.service';

@NgModule({
    declarations: [ ProfileComponent ],
    imports: [
        RouterModule,
        MatCardModule,
        MatInputModule,
        MatChipsModule,
        MatListModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        MatButtonModule,
        MatIconModule,
        MatTableModule,
        MatPaginatorModule
    ],
    providers: [
        EventService
    ],
    exports: [ ProfileComponent ]
})
export class ProfileModule {}
