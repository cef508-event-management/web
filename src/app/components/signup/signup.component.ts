import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';

import { UserService } from '../../services/user/user.service';
import { User } from '../../models/user';
import { MyErrorStateMatcher } from '../../components/login/login.component'

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  public user = {} as User;
  public matcher = new MyErrorStateMatcher();

  private emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);

  private passwordFormControl = new FormControl('', [
    Validators.required
  ])

  private nameFormControl = new FormControl('', [
    Validators.required
  ])

  constructor(
    private router: Router,
    private userService: UserService,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    console.log("Initializing signup component");
  }

  public signup() {
    if ( this.user.email && this.user['password'] && this.user.name
      && this.validateEmail(this.user.email) ) {

        this.userService.createUser(this.user)
        .subscribe( (res) => {
          this.displayResponse("Successfully created user account.")
          this.redirectToLogin()
        }, error => {
          console.log(error);
        });
    }
  }

  private validateEmail(email: string) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  private redirectToLogin() {
    this.router.navigate(["/login"]);
  }

  public displayResponse(message: string) {
    this.snackBar.open(message, "CLOSE", {
      duration: 5000,
    });
  }

}
