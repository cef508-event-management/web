import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
    MatCardModule,
    MatInputModule,
    MatChipsModule,
    MatListModule,
    MatButtonModule,
    MatSnackBarModule
 } from '@angular/material';

import { SignupComponent } from './signup.component';
import { UserService } from '../../services/user/user.service';

@NgModule({
    declarations: [ SignupComponent ],
    imports: [
        RouterModule,
        MatCardModule,
        MatInputModule,
        MatChipsModule,
        MatListModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        MatButtonModule,
        MatSnackBarModule
    ],
    providers: [
        UserService
    ],
    exports: [ SignupComponent ]
})
export class SignupModule {}