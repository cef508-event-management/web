import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import {
    MatGridListModule,
    MatCardModule,
    MatListModule,
    MatIconModule,
    MatButtonModule,
    MatDividerModule
} from '@angular/material';

import { EventsComponent } from './events.component';
import { EventService } from '../../services/event/event.service';
import { CategoryService } from '../../services/category/category.service';

@NgModule({
    declarations: [ EventsComponent ],
    imports: [
        BrowserModule,
        RouterModule,
        MatGridListModule,
        MatCardModule,
        MatListModule,
        MatIconModule,
        MatButtonModule,
        MatDividerModule
    ],
    providers: [
        EventService,
        CategoryService
    ],
    exports: [ EventsComponent ]
})
export class EventsModule {}
