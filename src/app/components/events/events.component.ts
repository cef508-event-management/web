import { Component, OnInit } from '@angular/core';
import { EventService } from '../../services/event/event.service';
import { Event } from '../../models/event';
import { Category } from '../../models/category';
import { CategoryService } from '../../services/category/category.service';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.css']
})
export class EventsComponent implements OnInit {
  events: Array<Event> = [];
  categories: Array<Category> = [];

  constructor(
    private eventService: EventService,
    private categoryService: CategoryService
  ) { }

  ngOnInit() {
    this.categoryService.getCategories().subscribe(data => {
      if (data) {
        this.categories = [ ...data ];
      }
    });

    this.eventService.getEvents().subscribe(data => {
      if (data) {
        this.events = [ ...data ];
      }
    });
  }
}
