export interface User {
    id: number;
    name: string;
    email: string;
    is_creator: boolean;
    created_at: number;
    updated_at: number;
}

