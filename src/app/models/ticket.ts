export interface Ticket {
    id: number;
    subcriber_id: number;
    event_id: number;
    created_at: number;
    updated_at: number;
}
