export interface Category {
    id: number;
    name: string;
    created_at: number;
    update_at: number;
}
