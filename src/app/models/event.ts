export interface Event {
    id: number;
    creator_id: number;
    category_id: number;
    event_name: string;
    event_date_time: number;
    location: string;
    capacity: number;
    gender_specification: string;
    age_group: string;
    description: string;
    caption: string;
    created_at: number;
    updated_at: number;
}
