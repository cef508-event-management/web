export interface Media {
    id: number;
    user_id: number;
    name: string;
    file_path: string;
    created_at: number;
    updated_at: number;
}
