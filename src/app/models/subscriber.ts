export interface Subscriber {
    id: number;
    user_id: number;
    gender: string;
    age_group: string;
    created_at: number;
    updated_at: number;
}
